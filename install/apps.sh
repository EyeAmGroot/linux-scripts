#!/bin/bash

set -eu
set -o pipefail

: ' Contains what is required to install my applications
TODO Seperate out adding stores and repositories and installing apps so that the system can be reloaded before moving on to user apps.
'

# Install stores & repositories
sudo apt install -y flatpak
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Install user apps

## Utils
sudo apt install -y curl

## Build chain
sudo apt install -y gcc g++ cmake
sudo apt install -y python3-pip

## Editing
sudo snap install --classic nvim
sudo snap install --classic ccls
curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install -y nodejs
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

python3 -m pip install --user --upgrade pynvim

sudo flatpak install flathub -y md.obsidian.Obsidian

## Cloud
sudo flatpak install flathub -y com.synology.SynologyDrive
