#!/bin/bash

set -eu
set -o pipefail

: '
Regolith is a combination of desktop environment tweaks with a tiling window manager.
'

sudo add-apt-repository -y ppa:regolith-linux/release
sudo apt install -y regolith-desktop-standard
sudo apt install -y i3xrocks-battery
