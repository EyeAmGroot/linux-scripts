# README #

This repository contains some scripts to make life easier on linux. In order for them to work they need to be placed in the home directory.
The "system-config" directory contains ordinary linux config files. In order to keep them synced between devices we need to create [symlinks](https://www.freecodecamp.org/news/symlink-tutorial-in-linux-how-to-create-and-remove-a-symbolic-link/) to them in the home directory.

```sh
ls -A system-config | xargs -I % sh -c 'ln -sf $(pwd)/system-config/% ~/%'
```

## TODO: 

Planned functionality:

- Add scripts to sync apps, settings, shortcuts and files across devices.
- Extend the functionality of the `open` command so that it interacts with a settings file which defines what are the default apps (nvim for txt, firefox for html, etc).
- Add inference of autocompletion to the cd command.
- Create aliases for shell colours
- Improve the bash prompt
  - Could include:
    - exit status
    - cwd
    - total files in directory
    - total filesize in directory
    - most recently used files in directory (replace the current printout after each cd command)
    - git branch (but only if an upper directory contains a .git file)
  - zsh, oh-my-zsh, powerlevel9k theme, nerd-fonts
    - https://medium.com/@alex285/get-powerlevel9k-the-most-cool-linux-shell-ever-1c38516b0caa
    - https://github.com/robbyrussell/oh-my-zsh
    - https://github.com/bhilburn.powerlevel9k
    - https://github.com/ryanoasis/nerd-fonts
    - http://bluejamesbond.github.io/CharacterMap
