#!/bin/bash

set -eu
set -o pipefail

: '
# Cope with a single CSV file per Excel sheet
# Folder for each Excel Sheet which stores CSV files
# Cope with cell merging somehow
# Parsing for formulae... Store formulae in seperate CSV files?
'

# Convert and deconvert between Excel sheet and CSV files when opening / saving
ssconvert $1 $1.csv

