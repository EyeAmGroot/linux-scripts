#!/bin/bash

set -eu
set -o pipefail

: ' Installs zsh as the default shell
'

sudo apt install -y zsh

# If zsh is not listed in shells, add it
if [[ ! 'grep '/usr/bin/zsh' /etc/shells' ]]; then
       echo "/usr/bin/zsh" | sudo tee -a /etc/shells > /dev/null
fi

# Install Oh-my-zsh and set zsh as the default shell
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
