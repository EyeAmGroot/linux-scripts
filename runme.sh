#!/bin/bash

set -eu
set -o pipefail

: ' 
Run this file to setup everything on a new linux machine automatically
'

ls -A system-config | xargs -I % sh -c 'ln -sf $(pwd)/system-config/% ~/%'
./install/apps.sh
./install/zsh.sh
./install/regolith.sh
./install/libs.sh
./config/environment.sh

